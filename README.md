# own Node.js STanDard library

Its a little library made for myself for better coding

## Installation

This project is not listed on NPM, so you need to install it via Git.

Via Git HTTP:

```bash
$ npm install git+https://gitlab.com/avolgha/nstd.git # if you prefer using npm
$ yarn add git+https://gitlab.com/avolgha/nstd.git    # if you prefer using yarnpkg
```

Via Git SSH:

```bash
$ npm install git+ssh://git@gitlab.com/avolgha/nstd.git # if you prefer using npm
$ yarn add git+ssh://git@gitlab.com/avolgha/nstd.git    # if you prefer using yarnpkg
```

## Current Features

### String Formatting

The library has the ability to format strings like you want. Currently, you can format strings with other strings, integers, doubles or booleans.

###### Example:

```typescript
import { format } from "nstd";

const input =
  "The brown {s} jumps {i} times over the dog with a height of {d,.2}";

const result = format(input, "fox", 5, 1.3058);
// Output: The brown fox jumps 5 times over the dog with a height of 1.31
```

| **Keyword** | **Options**                                                                                                         | **Types**                                        |
|:-----------:| ------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------ |
| `s`         | -/-                                                                                                                 | string, number (as string), boolean (as string)  |
| `i`         | -/-                                                                                                                 | string, number, boolean (0, 1)                   |
| `d`         | an integer >0, <1 that represents the amount of decimal places. Has to start with a dot (`.`). Default value is `2` | string, number, boolean (0.0, 1.0)               |
| `b`         | -/-                                                                                                                 | string ("true", "false"), number (0, 1), boolean |

You can define one of these tokens that way: `{keyword[,options]}`

The keyword is required by the formatter, the options not

#### Direct API Access

You can also access the tokenizer, parser and formatter via the direct file

```typescript
import { format, tokenize, parse, ... } from "nstd/build/format";
```

### I18N or Localization

The library has the ability to read and parse language files so you don't have to. Currently, only YAML files are supported that ends with `.yaml`.

###### Example:

```typescript
import { useLanguage, LanguageOptions } from "nstd";
import path from "path";

const options: LanguageOptions = {
    dir: path.join(__dirname, "..", "lang"),
    default: "en",
    current: "de",
};

const client = useLanguage(options);
const { log } = console;

log(`[${client.language}] Greeting: ${client.get("msg.greeting")}`);

client.language = "en";
log(`[${client.language}] Greeting: ${client.get("msg.greeting")}`);

/*
With the right language files, it should produce this:

[de] Greeting: Hallo, Welt!
[en] Greeting: Hello, World!
*/
```

**Warning**:

If you set a wrong language and the client tries to load this file, it will generate an error and shut down the process

#### API

**LanguageOptions**:

| **Name** | **Type**        | **Description**                                                                                                       |
| -------- | --------------- | --------------------------------------------------------------------------------------------------------------------- |
| dir      | readonly string | The directory you want to store the language files in                                                                 |
| default  | string          | The default language that will be also used as current language and as language for the reset method. Default to `en` |
| current  | string          | The current language. Default is the `default` language                                                               |

**LanguageClient**:

| **Name**   | **Type**                        | **Description**                                                                                                  |
| ---------- | ------------------------------- | ---------------------------------------------------------------------------------------------------------------- |
| file       | LanguageFile                    | File store of the language file                                                                                  |
| options    | readonly LanguageOptions        | The options that were parsed into the client                                                                     |
| reload     | () => void                      | Reloads the messages in the language file                                                                        |
| reset      | () => void                      | Set the current language to the default language and reload the language file                                    |
| language   | set (string) => void            | Set the current language                                                                                         |
| language   | get () => string                | Returns the current language                                                                                     |
| get        | (string) => string \| undefined | Returns the message by it's key from the language file. If it does not exists, returns `undefined`               |
| getNonNull | (string) => string              | Returns the message by it's key from the language file. If it does not exists, the functions will throw an error |

### Console

### Event Manager

### Stack
