export const formatRegex: RegExp = new RegExp(/{[s,i,d,b](,([^}]+))?}/gm);

/**
 * The type of the token
 */
export type FormatTokenType = "string" | "integer" | "double" | "boolean";

/**
 * Interface for defining a token
 */
export interface FormatToken {
  readonly raw: string;
  readonly type: FormatTokenType;
  readonly options: string;
  readonly startIndex: number;
  readonly endIndex: number;
}

/**
 * The functions returns the type of the token.
 *
 * This can produce an error if the type is not a regular token type
 *
 * @param raw {String} The raw token
 * @returns {FormatTokenType} The token type
 */
export function getType(raw: string): FormatTokenType {
  switch (raw.charAt(1)) {
    case "s":
      return "string";
    case "i":
      return "integer";
    case "d":
      return "double";
    case "b":
      return "boolean";
  }
  throw new Error(`unreachable`);
}

/**
 * The functions returns the options of the token
 *
 * @param raw {String} The raw token
 * @returns {String} The token options
 */
export function getOptions(raw: string): string {
  return raw.slice("{x,".length, raw.length - 1);
}

/**
 * The functions returns the tokens from the given input as an array
 *
 * @param input {String} The raw input that you want to get the tokens from
 * @returns {FormatToken[]} An array of tokens
 */
export function tokenize(input: string): FormatToken[] {
  const result = input.match(formatRegex);
  if (result === null) return [];
  const tokenList: FormatToken[] = [];
  let lastToken = 0;
  for (const token of result) {
    const start = input.indexOf(token, lastToken);
    const end = start + token.length;
    tokenList.push({
      raw: token,
      type: getType(token),
      options: getOptions(token),
      startIndex: start,
      endIndex: end,
    });
    lastToken = end;
  }
  return tokenList;
}

/**
 * The functions returns you an formatted object for the provided token
 *
 * This can produce an error if the type is not a regular token type or
 * the object cannot be converted
 *
 * @param object {any} The object that should be formatted
 * @param token {FormatToken} The token you want to convert the object for
 * @returns {any} The formatted object
 */
export function formatObject(object: any, token: FormatToken): any {
  switch (token.type) {
    case "string": {
      return object + "";
    }
    case "integer": {
      if (typeof object === "number") {
        return object;
      }

      if (typeof object === "boolean") {
        return object === true ? 1 : 0;
      }

      if (typeof object === "string") {
        return parseInt(object);
      }

      throw new Error(
        `Cannot convert object of type '${typeof object}' (${object}) to an integer.`
      );
    }
    case "double": {
      const decimalPlaces = ((opt: string) => {
        if (opt === "") {
          return 2;
        }

        if (!token.options.startsWith(".")) {
          throw new Error(
            `Double option for rounding the number has to start with a dot ('.').`
          );
        }

        return parseInt(token.options.slice(1));
      })(token.options);

      try {
        if (typeof object === "number") {
          return object.toFixed(decimalPlaces);
        }

        if (typeof object === "string") {
          return parseInt(object).toFixed(decimalPlaces);
        }

        throw new Error(
          `Cannot convert object of type '${typeof object}' (${object}) to a double.`
        );
      } catch (error) {
        throw new Error(`Cannot convert double: ${error}`);
      }
    }
    case "boolean": {
      if (typeof object === "boolean") {
        return object === true ? "true" : "false";
      }

      if (typeof object === "number" && (object === 0 || object === 1)) {
        return object === 0 ? "false" : "true";
      }

      if (
        typeof object === "string" &&
        (object === "true" || object === "false")
      ) {
        return object === "true";
      }

      throw new Error(
        `Cannot convert object of type '${typeof object}' (${object}) to a boolean.`
      );
    }
  }

  throw new Error(`Unknown token type: ${token.type}`);
}

/**
 * The function takes the objects and the tokens and combines them into the
 * input string
 *
 * @param input {String} The raw input string
 * @param objects {any[]} The objects you want to parse in
 * @param tokenList {FormatToken[]} The tokens you want to use for parsing
 * @returns {String} The formatted input string
 */
export function parse(
  input: string,
  objects: any[],
  tokenList: FormatToken[]
): string {
  let newString = input;
  let index = 0;
  tokenList.forEach((token) => {
    if (index >= objects.length) return;
    newString = newString.replace(
      token.raw,
      formatObject(objects[index++], token)
    );
  });
  return newString;
}

/**
 * The function will take your ojects and parses them into
 * the input string
 *
 * Short hand function to call the tokenizer and the parser
 *
 * @param input {String} The input string
 * @param objects {...any} The objects you want to parse in
 * @returns {String} The formatted string
 */
export default function format(input: string, ...objects: any): string {
  return parse(input, objects, tokenize(input));
}
