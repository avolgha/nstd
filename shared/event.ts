/**
 * Data of an event. Represented here as a string
 */
export type EventData = string;

/**
 * Event Handler interface for no special purpose
 */
export interface EventHandler {
  /**
   * The event that the handler is representing
   */
  event: string;

  /**
   * Call the event with provided data
   *
   * @param data The data parsed into the event
   */
  call(data: EventData): void;
}

/**
 * Decleration interface for knowing what an event manager is
 */
export interface EventManager {
  /**
   * Register a handler for a specific event
   *
   * @param event The event you want to listen on
   * @param callback Function for showing what you want to do with the data
   */
  on(event: string, callback: (data: EventData) => void): void;

  /**
   * Call a event
   *
   * @param event The event you want to call
   * @param data The data you want to parse into the event
   */
  call(event: string, data: EventData): void;

  /**
   * Get all registered handlers of a specific event
   *
   * @param event The event you want to get the handlers of
   */
  get(event: string): EventHandler[] | undefined;
}

/**
 * Create a simple event manager
 *
 * @returns The created event manager
 */
export default function makeManager(): EventManager {
  class EventManagerImpl implements EventManager {
    private map = new Map<string, EventHandler[]>();

    on(event: string, callback: (data: string) => void): void {
      if (!this.map.has(event)) return;
      const current = this.map.get(event)!;
      current.push({
        event,
        call: callback,
      });
      this.map.set(event, current);
    }

    call(event: string, data: EventData): void {
      if (!this.map.has(event)) return;
      this.get(event)!.forEach((handler) => handler.call(data));
    }

    get(event: string): EventHandler[] | undefined {
      return this.map.get(event);
    }
  }

  return new EventManagerImpl();
}
