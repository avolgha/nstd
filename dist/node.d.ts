export interface FactoryOptions {
  id?: string;
  classes?: string[];
  attributes?: { [name: string]: any };
  events?: { [name: string]: (event: any) => void };
}

export interface EventManager {
  /**
   * Register a handler for a specific event
   *
   * @param event The event you want to listen on
   * @param callback Function for showing what you want to do with the data
   */
  on(event: string, callback: (data: EventData) => void): void;

  /**
   * Call a event
   *
   * @param event The event you want to call
   * @param data The data you want to parse into the event
   */
  call(event: string, data: EventData): void;

  /**
   * Get all registered handlers of a specific event
   *
   * @param event The event you want to get the handlers of
   */
  get(event: string): EventHandler[] | undefined;
}

/**
 * Create a simple event manager
 *
 * @returns The created event manager
 */
export function createEventManager(): EventManager {}

/**
 * The function will take your ojects and parses them into
 * the input string
 *
 * Short hand function to call the tokenizer and the parser
 *
 * @param input {String} The input string
 * @param objects {...any} The objects you want to parse in
 * @returns {String} The formatted string
 */
export function fmt(input: string, ...objects: any): string;

/**
 * Class for creating a stack
 */
export class Stack<T> {
  /**
   * @returns {T} Returns the current value from the stack
   */
  current(): T;

  /**
   * Put a value to the start of the stack
   *
   * @param value The value you want to push ontop
   */
  pushTop(value: T): void;

  /**
   * Put a value to the end of the stack
   *
   * @param value The value you want to push to the end
   */
  pushEnd(value: T): void;

  /**
   * Reverse the stack
   */
  reverseStack(): void;

  /**
   * Removes the first element from the stack
   *
   * @returns The shiftet value
   */
  shiftFirst(): T;

  /**
   * Removes the last element from the stack
   *
   * @returns The shiftet value
   */
  shiftLast(): T;

  /**
   * Clears the stack
   */
  clear(): void;

  /**
   * Get the length of the stack
   */
  get length(): number;
}

/**
 * Abstract class to define the structure of a language client
 *
 * For implementation see the `use` function
 */
export abstract class LanguageClient {
  file: LanguageFile;

  abstract reload(): void;
  abstract reset(): void;
  abstract set language(newLanguage: string);
  abstract get language();
  abstract get(key: string): string | undefined;
  abstract getNonNull(key: string): string;
}

/**
 * Options used in the parser
 */
export interface LanguageOptions {
  readonly dir: string;
  default?: string;
  current?: string;
}

/**
 * Basic implementation of the language client
 *
 * @param options The options for the language client
 * @returns The language client itself
 */
export function use(options: LanguageOptions): LanguageClient;

export const colors: {
  reset: (text: string) => string;
  bold: (text: string) => string;
  dim: (text: string) => string;
  italic: (text: string) => string;
  underline: (text: string) => string;
  inverse: (text: string) => string;
  hidden: (text: string) => string;
  strikethrough: (text: string) => string;
  black: (text: string) => string;
  red: (text: string) => string;
  green: (text: string) => string;
  yellow: (text: string) => string;
  blue: (text: string) => string;
  magenta: (text: string) => string;
  cyan: (text: string) => string;
  white: (text: string) => string;
  gray: (text: string) => string;
  grey: (text: string) => string;
  bgBlack: (text: string) => string;
  bgRed: (text: string) => string;
  bgGreen: (text: string) => string;
  bgYellow: (text: string) => string;
  bgBlue: (text: string) => string;
  bgMagenta: (text: string) => string;
  bgCyan: (text: string) => string;
  bgWhite: (text: string) => string;
};
