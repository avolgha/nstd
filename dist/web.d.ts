interface FactoryOptions {
  id?: string;
  classes?: string[];
  attributes?: { [name: string]: any };
  events?: { [name: string]: (event: any) => void };
}

interface EventManager {
  /**
   * Register a handler for a specific event
   *
   * @param event The event you want to listen on
   * @param callback Function for showing what you want to do with the data
   */
  on(event: string, callback: (data: EventData) => void): void;

  /**
   * Call a event
   *
   * @param event The event you want to call
   * @param data The data you want to parse into the event
   */
  call(event: string, data: EventData): void;

  /**
   * Get all registered handlers of a specific event
   *
   * @param event The event you want to get the handlers of
   */
  get(event: string): EventHandler[] | undefined;
}

/**
 * Create a simple event manager
 *
 * @returns The created event manager
 */
export function createEventManager(): EventManager {}

/**
 * The function will take your ojects and parses them into
 * the input string
 *
 * Short hand function to call the tokenizer and the parser
 *
 * @param input {String} The input string
 * @param objects {...any} The objects you want to parse in
 * @returns {String} The formatted string
 */
export function fmt(input: string, ...objects: any): string;

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: string,
  options?: FactoryOptions
): HTMLElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "html",
  options?: FactoryOptions
): HTMLHtmlElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "base",
  options?: FactoryOptions
): HTMLBaseElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "head",
  options?: FactoryOptions
): HTMLHeadElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "link",
  options?: FactoryOptions
): HTMLLinkElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "meta",
  options?: FactoryOptions
): HTMLMetaElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "style",
  options?: FactoryOptions
): HTMLStyleElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "title",
  options?: FactoryOptions
): HTMLTitleElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "body",
  options?: FactoryOptions
): HTMLBodyElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "h1" | "h2" | "h3" | "h4" | "h5" | "h6",
  options?: FactoryOptions
): HTMLHeadingElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "div",
  options?: FactoryOptions
): HTMLDivElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "dl",
  options?: FactoryOptions
): HTMLDListElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "li",
  options?: FactoryOptions
): HTMLLIElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "ol",
  options?: FactoryOptions
): HTMLOListElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "p",
  options?: FactoryOptions
): HTMLParagraphElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "pre",
  options?: FactoryOptions
): HTMLPreElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "ul",
  options?: FactoryOptions
): HTMLUListElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "a",
  options?: FactoryOptions
): HTMLAnchorElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "br",
  options?: FactoryOptions
): HTMLBRElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "data",
  options?: FactoryOptions
): HTMLDataElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "area",
  options?: FactoryOptions
): HTMLAreaElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "audio",
  options?: FactoryOptions
): HTMLAudioElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "img",
  options?: FactoryOptions
): HTMLImageElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "map",
  options?: FactoryOptions
): HTMLMapElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "track",
  options?: FactoryOptions
): HTMLTrackElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "video",
  options?: FactoryOptions
): HTMLVideoElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "embed",
  options?: FactoryOptions
): HTMLEmbedElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "iframe",
  options?: FactoryOptions
): HTMLIFrameElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "object",
  options?: FactoryOptions
): HTMLObjectElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "param",
  options?: FactoryOptions
): HTMLParamElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "picture",
  options?: FactoryOptions
): HTMLPictureElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "source",
  options?: FactoryOptions
): HTMLSourceElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "svg",
  options?: FactoryOptions
): HTMLOrSVGElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "canvas",
  options?: FactoryOptions
): HTMLCanvasElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "script",
  options?: FactoryOptions
): HTMLScriptElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "button",
  options?: FactoryOptions
): HTMLButtonElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "datalist",
  options?: FactoryOptions
): HTMLDataListElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "fieldset",
  options?: FactoryOptions
): HTMLFieldSetElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "form",
  options?: FactoryOptions
): HTMLFormElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "input",
  options?: FactoryOptions
): HTMLInputElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "label",
  options?: FactoryOptions
): HTMLLabelElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "legend",
  options?: FactoryOptions
): HTMLLegendElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "meter",
  options?: FactoryOptions
): HTMLMeterElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "optgroup",
  options?: FactoryOptions
): HTMLOptGroupElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "option",
  options?: FactoryOptions
): HTMLOptionElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "output",
  options?: FactoryOptions
): HTMLOutputElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "progress",
  options?: FactoryOptions
): HTMLProgressElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "select",
  options?: FactoryOptions
): HTMLSelectElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "textarea",
  options?: FactoryOptions
): HTMLTextAreaElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "details",
  options?: FactoryOptions
): HTMLDetailsElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "dialog",
  options?: FactoryOptions
): HTMLDialogElement {}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export function createElement(
  label: "menu",
  options?: FactoryOptions
): HTMLMenuElement {}
