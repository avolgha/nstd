/**
 * Class for creating a stack
 */
export default class Stack<T> {
  private stack: T[] = [];

  constructor(...initialValues: []) {
    this.stack = initialValues;
  }

  /**
   * @returns {T} Returns the current value from the stack
   */
  current(): T {
    return this.stack[0];
  }

  /**
   * Put a value to the start of the stack
   *
   * @param value The value you want to push ontop
   */
  pushTop(value: T) {
    this.stack = [value, ...this.stack];
  }

  /**
   * Put a value to the end of the stack
   *
   * @param value The value you want to push to the end
   */
  pushEnd(value: T) {
    this.stack.push(value);
  }

  /**
   * Reverse the stack
   */
  reverseStack() {
    this.stack = this.stack.reverse();
  }

  /**
   * Removes the first element from the stack
   *
   * @returns The shiftet value
   */
  shiftFirst() {
    return this.stack.shift();
  }

  /**
   * Removes the last element from the stack
   *
   * @returns The shiftet value
   */
  shiftLast() {
    return this.stack.pop();
  }

  /**
   * Clears the stack
   */
  clear() {
    this.stack = [];
  }

  /**
   * Get the length of the stack
   */
  get length() {
    return this.stack.length;
  }
}
