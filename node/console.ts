import { format, colors } from "./node";

function createConsole() {
  const streams: {
    input: NodeJS.ReadableStream;
    output: NodeJS.WritableStream;
    error: NodeJS.WritableStream;
  } = {
    input: process.stdin,
    output: process.stdout,
    error: process.stderr,
  };

  /**
   * Print a raw message to the given writale stream
   *
   * @param message The message you want to print
   * @param stream The stream you want to print to. Default is stdout
   */
  function print(
    message: string,
    stream: NodeJS.WritableStream = streams.output
  ) {
    stream.write(`${message}\n`);
  }

  /**
   * Print an information message to stdout
   *
   * @param message The message you want to print
   * @param objects The objects you want to format the message with
   */
  function info(message: string, ...objects: any) {
    print(
      `${colors.bgGreen("INFO")}${" " + colors.white(format(message, objects))}`
    );
  }

  /**
   * Print a debug message to stdout
   *
   * @param message The message you want to print
   * @param objects The objects you want to format the message with
   */
  function debug(message: string, ...objects: any) {
    print(
      `${colors.bgCyan("DEBUG")}${" " + colors.white(format(message, objects))}`
    );
  }

  /**
   * Print a warning message to stdout
   *
   * @param message The message you want to print
   * @param objects The objects you want to format the message with
   */

  function warn(message: string, ...objects: any) {
    print(
      `${colors.bgYellow("WARN")}${
        " " + colors.white(format(message, objects))
      }`
    );
  }

  /**
   * Print an error message to stderr
   *
   * @param message The message you want to print
   * @param objects The objects you want to format the message with
   */

  function error(message: string, ...objects: any) {
    print(
      `${colors.bgRed("ERROR")}${" " + colors.white(format(message, objects))}`,
      streams.error
    );
  }

  /**
   * Get informaton via stdin from the user
   *
   * @param options Options of the input function
   * @returns Returns the input that was provided by the user and validated by the given validator
   */
  function input(options?: {
    question?: string;
    validator?: (input: string) => boolean;
  }) {
    if (!options || !options.validator)
      options = { ...options, validator: (input) => input !== "" };
    const { input } = streams;
    let result: string;
    do {
      if (options.question) {
        process.stdout.write(`${options.question} `);
      }
      result = (() => {
        const inputResult = input.read();
        if (typeof inputResult === "string") {
          return inputResult;
        }

        return inputResult.toString("utf-8");
      })();
      //@ts-ignore
    } while (!options.validator(result));
    return result;
  }

  return {
    print,
    info,
    debug,
    warn,
    error,
    input,
  };
}

/**
 * Default outputter for NSTD
 */
export const output = createConsole();
