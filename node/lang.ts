import fs from "fs";
import path from "path";
import yaml from "yaml";

/**
 * Interface for defining what a language buffer is
 */
export interface Language {
  readonly [key: string]: string;
}

/**
 * Interface for defining the file of the language
 */
export interface LanguageFile {
  readonly path: string;
  readonly content: Language;
  reload(): void;
}

/**
 * Options used in the parser
 */
export interface LanguageOptions {
  readonly dir: string;
  default?: string;
  current?: string;
}

class LanguageFileObject implements LanguageFile {
  content: Language;

  constructor(public readonly path: string) {
    if (!fs.existsSync(path)) {
      throw new Error(`Cannot resolve language file at '${path}'.`);
    }

    this.content = yaml.parse(fs.readFileSync(path, { encoding: "utf-8" }));
  }

  reload(): void {
    this.content = yaml.parse(
      fs.readFileSync(this.path, { encoding: "utf-8" })
    );
  }
}

/**
 * Abstract class to define the structure of a language client
 *
 * For implementation see the `use` function
 */
export abstract class LanguageClient {
  file: LanguageFile;

  constructor(public readonly options: LanguageOptions) {
    if (options.default === undefined) options.default = "en";
    if (options.current === undefined) options.current = options.default;

    const filePath = path.join(options.dir, `${options.current}.yaml`);
    if (!fs.existsSync(filePath)) {
      throw new Error(`Cannot resolve language file at '${filePath}'.`);
    }

    this.file = new LanguageFileObject(filePath);
  }

  abstract reload(): void;
  abstract reset(): void;
  abstract set language(newLanguage: string);
  abstract get language();
  abstract get(key: string): string | undefined;
  abstract getNonNull(key: string): string;
}

/**
 * Basic implementation of the language client
 *
 * @param options The options for the language client
 * @returns The language client itself
 */
export default function use(options: LanguageOptions): LanguageClient {
  class LanguageClientImpl extends LanguageClient {
    private currentLanguage = options.current as any as string; // cannot be undefined anymore

    set language(newLanguage: string) {
      this.currentLanguage = newLanguage;
      this.reload();
    }

    get language(): string {
      return this.currentLanguage;
    }

    reload(): void {
      this.file = new LanguageFileObject(
        path.join(options.dir, `${this.currentLanguage}.yaml`)
      );
    }

    reset(): void {
      this.currentLanguage = options.default as any as string;
      this.reload();
    }

    get(key: string): string | undefined {
      return this.file.content[key];
    }

    getNonNull(key: string): string {
      const value = this.get(key);
      if (value !== undefined) {
        return value;
      }
      throw new Error(
        `Cannot find language key '${key}' (${this.currentLanguage})`
      );
    }
  }
  return new LanguageClientImpl(options);
}
