function when(object: any, func: (object: any) => void) {
  if (object !== undefined) func(object);
}

interface FactoryOptions {
  id?: string;
  classes?: string[];
  attributes?: { [name: string]: any };
  events?: { [name: string]: (event: any) => void };
}

/**
 * Create a element by a label with the specified options
 *
 * @param label The keyword of the html element
 * @param options The options of the element
 * @returns Returns the created Element
 */
export default function createElement(
  label: string,
  options?: FactoryOptions
): HTMLElement {
  const element = document.createElement(label);
  when(options, (object: FactoryOptions) => {
    const { id, classes, attributes, events } = object;
    when(id, () => (element.id = id!));
    when(classes, () =>
      classes!.forEach((classObj) => element.classList.add(classObj))
    );
    when(attributes, () => {
      for (const key in attributes) {
        element.setAttribute(key, attributes[key]);
      }
    });
    when(events, () => {
      for (const key in events) {
        //@ts-ignore
        when(element[`on${key}`], () => (element[`on${key}`] = events[key]));
      }
    });
  });
  return element;
}
