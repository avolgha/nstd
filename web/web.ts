export { default as createEventManager } from "../shared/event";
export { default as fmt } from "../shared/format";

export { default as createElement } from "./factory";
